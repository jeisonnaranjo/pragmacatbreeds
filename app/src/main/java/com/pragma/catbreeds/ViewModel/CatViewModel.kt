package com.pragma.catbreeds.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pragma.catbreeds.Domain.GetCatListUseCase
import com.pragma.catbreeds.Model.CatModel
import kotlinx.coroutines.launch

class CatViewModel : ViewModel() {

    val catModel = MutableLiveData<List<CatModel>?>()

    var getCatListUseCase = GetCatListUseCase()

    fun onCreate(postUrl: String) {
        viewModelScope.launch {
            val result : List<CatModel>? = getCatListUseCase(postUrl)
            if (result != null){
                catModel.postValue(result)
            }
        }
    }


    fun getDataModel(){
        /*val currentCat = CatProvider.getCatRamdom()
        catModel.postValue(currentCat)*/
    }


}
