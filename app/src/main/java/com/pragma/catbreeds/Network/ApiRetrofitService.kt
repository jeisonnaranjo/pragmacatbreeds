package com.pragma.catbreeds.Network

import com.pragma.catbreeds.Model.CatModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Url

interface ApiRetrofitService {
    @Headers("x-api-key: bda53789-d59e-46cd-9bc4-2936630fde39")
    @GET
    suspend fun getListCats(@Url url : String) : Response<List<CatModel>>
}