package com.pragma.catbreeds.Network

import com.pragma.catbreeds.Model.CatModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NetworkService {

    suspend fun getCatList(postUrl: String): List<CatModel> {
        return withContext(Dispatchers.IO){
            val request = NetworkRetrofit.getNetworkRetrofit()
                .create(ApiRetrofitService::class.java)
                .getListCats(postUrl)
            request.body() ?: emptyList()
        }
    }
}