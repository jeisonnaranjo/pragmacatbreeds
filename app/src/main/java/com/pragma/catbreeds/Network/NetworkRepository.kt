package com.pragma.catbreeds.Network

import com.pragma.catbreeds.Model.CatModel
import com.pragma.catbreeds.Model.CatProvider

class NetworkRepository {

    private val api = NetworkService()

    suspend fun getCatList(postUrl: String) : List<CatModel>{
        val response = api.getCatList(postUrl)
        CatProvider.listCats = response
        return response
    }
}