package com.pragma.catbreeds.Network

import com.pragma.catbreeds.Utils.Const
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkRetrofit {
    companion object{
        fun getNetworkRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(Const.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

}