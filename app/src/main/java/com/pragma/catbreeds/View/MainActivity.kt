package com.pragma.catbreeds.View

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.pragma.catbreeds.Adapter.CatAdapter
import com.pragma.catbreeds.Model.CatModel
import com.pragma.catbreeds.Utils.Const
import com.pragma.catbreeds.Utils.Util
import com.pragma.catbreeds.ViewModel.CatViewModel
import com.pragma.catbreeds.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    private lateinit var viewBinding: ActivityMainBinding


    private val catViewModel: CatViewModel by viewModels()

    private lateinit var catAdapter: CatAdapter
    private var listCatModels = mutableListOf<CatModel>()

    override fun onCreate(savedInstanceState: Bundle?) {

        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        splashScreen.setKeepOnScreenCondition { false }

        initUI()

        searchBreed("")

        catViewModel.catModel.observe(this, Observer { listCatModelsO ->
            if (listCatModelsO != null) {
                if (listCatModelsO.isNotEmpty()) {
                    listCatModels.clear()
                    listCatModels.addAll(listCatModelsO)
                    catAdapter.notifyDataSetChanged()
                    if (listCatModels.isEmpty()) {
                        Util().showToast("Sin resultados", Const.INFO, applicationContext)
                    } else {
                        Util().showToast("Ok", Const.SUCCESFULL, applicationContext)
                    }
                } else {
                    Util().showToast("Hubo un error en la solicitud de los datos", Const.ERROR, applicationContext)
                }
            }
            viewBinding.shimmerCatList.visibility = View.GONE
            viewBinding.rcvCatList.visibility = View.VISIBLE
            hideKB()
        })
    }

    private fun initUI() {
        viewBinding.edtSearch.setOnQueryTextListener(this)
        catAdapter = CatAdapter(listCatModels, itemClickListener = { catCliked -> onItemClickListener(catCliked) })
        viewBinding.rcvCatList.layoutManager = LinearLayoutManager(this)
        viewBinding.rcvCatList.adapter = catAdapter
    }

    fun onItemClickListener(catModelCliked: CatModel) {
        val intent = Intent(this, DetailActivity::class.java)
        /*var bundle = Bundle()
        bundle.putSerializable()*/
        intent.putExtra(Const.CAT, catModelCliked)
        startActivity(intent)
    }

    private fun searchBreed(search: String) {
        viewBinding.shimmerCatList.visibility = View.VISIBLE
        viewBinding.rcvCatList.visibility = View.GONE

        var postUrl = if (search.isNullOrEmpty()) "" else "search?q=$search"
        catViewModel.onCreate(postUrl)
    }

    private fun hideKB() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(viewBinding.ctlContentPrincipal.windowToken, 0)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (!query.isNullOrEmpty()) {
            searchBreed(search = query.lowercase())
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null) {
            if (newText.isBlank()) {
                searchBreed(search = "")
            }
        }
        return true
    }
}