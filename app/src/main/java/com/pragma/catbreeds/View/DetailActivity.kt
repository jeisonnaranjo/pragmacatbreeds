package com.pragma.catbreeds.View

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.pragma.catbreeds.Model.CatModel
import com.pragma.catbreeds.R
import com.pragma.catbreeds.Utils.Const
import com.pragma.catbreeds.Utils.Util
import com.pragma.catbreeds.databinding.ActivityDetailBinding


class DetailActivity : AppCompatActivity(), View.OnClickListener, View.OnTouchListener {

    private lateinit var viewBinding: ActivityDetailBinding

    private lateinit var catModel: CatModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        catModel = intent?.getSerializableExtra(Const.CAT) as CatModel
        initUI()
    }

    private fun initUI() {
        setSupportActionBar(viewBinding.toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        viewBinding.toolbar.setNavigationIcon(R.drawable.atras)
        viewBinding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        if (catModel != null) {

            viewBinding.txvBreedCat.text = catModel.name

            if (!catModel.reference_image_id.isNullOrEmpty()) {
                Glide.with(this)
                    .load(Const.URL_BASE_IMAGE_CAT + catModel.reference_image_id + ".jpg")
                    .placeholder(R.drawable.gato)
                    .into(viewBinding.imvImageCat)
            }

            viewBinding.txvDescriptionCat.text = catModel?.description

            //Se pinta la bandera del pais del gato usando una ruta tomada de la documentacion y usamos libreria GlideToVectorYou para traer la imagen svg y pintarla en el ImageView
            //ej ruta bandera: //https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.1/flags/1x1/gb.svg
            var uri: Uri? = Uri.parse(Const.URL_BASE_IMAGE_COUNTRY + catModel?.country_code.lowercase() + ".svg")
            GlideToVectorYou
                .init()
                .with(this)
                .load(uri, viewBinding.imvImageCountry)
            viewBinding.txvCounrtyCat.text = "País de origen:  " + catModel?.origin

            Glide.with(this)
                .load(R.drawable.life)
                .placeholder(R.drawable.gato)
                .into(viewBinding.imvImageLifeSpan)
            viewBinding.txvLifeSpanCat.text = "Tiempo de vida:  " + catModel?.life_span

            viewBinding.txvIntelligenceCat.text = "Inteligencia:  " + catModel?.intelligence.toString()
            viewBinding.skbIntelligenceCat.progress = catModel?.intelligence
            viewBinding.skbIntelligenceCat.setOnTouchListener(this)

            viewBinding.txvAdaptabilidadCat.text = "Adaptabilidad:  " + catModel?.adaptability.toString()
            viewBinding.skbAdaptabilidadCat.progress = catModel?.adaptability
            viewBinding.skbAdaptabilidadCat.setOnTouchListener(this)

            viewBinding.txvHipoalergenicoCat.text = "Hipoalergénico:  " + catModel?.hypoallergenic.toString()
            viewBinding.skbHipoalergenicoCat.progress = catModel?.hypoallergenic
            viewBinding.skbHipoalergenicoCat.setOnTouchListener(this)

            viewBinding.txvChildFriendlyCat.text = "Apto para nilos:  " + catModel?.child_friendly.toString()
            viewBinding.skbChildFriendlyCat.progress = catModel?.child_friendly
            viewBinding.skbChildFriendlyCat.setOnTouchListener(this)

            viewBinding.txvEnergyLevelCat.text = "Nivel de energia:  " + catModel?.child_friendly.toString()
            viewBinding.skbEnergyLevelCat.progress = catModel?.child_friendly
            viewBinding.skbEnergyLevelCat.setOnTouchListener(this)

            viewBinding.btnWikipedia.setOnClickListener(this)
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnWikipedia -> {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(catModel?.wikipedia_url)
                startActivity(intent)
            }
            else -> {
                Util().showToast("No action implements", Const.ERROR, applicationContext)
            }
        }
    }

    override fun onTouch(view: View?, p1: MotionEvent?): Boolean {
        return true
    }
}


