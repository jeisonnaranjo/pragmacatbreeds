package com.pragma.catbreeds.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Weight (
    @SerializedName("imperial") var imperial : String,
    @SerializedName("metric") var metric : String
) : Serializable