package com.pragma.catbreeds.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Image (
    @SerializedName("id") var id : String,
    @SerializedName("width") var width : Int,
    @SerializedName("height") var height : Int,
    @SerializedName("url") var url : String,
) : Serializable