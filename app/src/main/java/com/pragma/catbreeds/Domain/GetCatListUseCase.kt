package com.pragma.catbreeds.Domain

import com.pragma.catbreeds.Model.CatModel
import com.pragma.catbreeds.Network.NetworkRepository

class GetCatListUseCase {

    private val repository = NetworkRepository()

    suspend operator fun invoke(postUrl: String):List<CatModel>?{
        return repository.getCatList(postUrl)
    }
}