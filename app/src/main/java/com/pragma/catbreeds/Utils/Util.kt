package com.pragma.catbreeds.Utils

import android.content.Context
import android.widget.Toast
import es.dmoral.toasty.Toasty

public class Util {

    fun showToast(message: String, typeMessage: Int, applicationContext : Context) {
        when (typeMessage) {
            Const.SUCCESFULL -> Toasty.success(applicationContext, message, Toast.LENGTH_SHORT, true).show();
            Const.WARNING -> Toasty.warning(applicationContext, message, Toast.LENGTH_SHORT, true).show();
            Const.ERROR -> Toasty.error(applicationContext, message, Toast.LENGTH_SHORT, true).show();
            Const.INFO -> Toasty.info(applicationContext, message, Toast.LENGTH_SHORT, true).show();
            else -> Toasty.info(applicationContext, message, Toast.LENGTH_SHORT, true).show();
        }
    }
}