package com.pragma.catbreeds.Utils

object Const {
    const val URL_BASE : String = "https://api.thecatapi.com/v1/breeds/"
    const val URL_BASE_IMAGE_CAT : String = "https://cdn2.thecatapi.com/images/"
    const val URL_BASE_IMAGE_COUNTRY : String = "https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.1/flags/1x1/"
    const val SUCCESFULL: Int = 1
    const val WARNING: Int = 2
    const val ERROR: Int = 3
    const val INFO: Int = 4
    const val CAT : String = "CAT"
}