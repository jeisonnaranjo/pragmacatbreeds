package com.pragma.catbreeds.Adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.pragma.catbreeds.Model.CatModel
import com.pragma.catbreeds.Utils.Const
import com.pragma.catbreeds.R
import com.pragma.catbreeds.databinding.ItemCatRcvBinding


class CatAdapter(val catModelList: List<CatModel>, private val itemClickListener : (CatModel) -> Unit) : RecyclerView.Adapter<CatAdapter.CatViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_cat_rcv, parent, false)
        return CatViewHolder(view)
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        val catModel: CatModel = catModelList[position]
        holder.loadCat(catModel, itemClickListener)
    }

    override fun getItemCount(): Int {
        return catModelList.size
    }


    class CatViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val viewBinding = ItemCatRcvBinding.bind(view)
        fun loadCat(catModel: CatModel, itemClickListener : (CatModel) -> Unit) {
            viewBinding.txvBreedCat.text = catModel.name
            viewBinding.txvCounrtyCat.text = catModel.origin
            viewBinding.txvIntelligenceCat.text = "Inteligencia: "+catModel.intelligence.toString()
            if (!catModel.reference_image_id.isNullOrEmpty()){
                Glide.with(view.context)
                    .load(Const.URL_BASE_IMAGE_CAT + catModel.reference_image_id+".jpg")
                    .placeholder(R.drawable.gato)
                    .into(viewBinding.imvImageCat)
            }

            if (!catModel.country_code.isNullOrEmpty()){
                //Se pinta la bandera del pais del gato usando una ruta tomada de la documentacion ej: //https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.1/flags/1x1/gb.svg
                var uri : Uri? = Uri.parse(Const.URL_BASE_IMAGE_COUNTRY + catModel.country_code.lowercase()+".svg")
                GlideToVectorYou
                    .init()
                    .with(view.context)
                    .load(uri, viewBinding.imvImageCountry);
            }


            itemView.setOnClickListener(){
                itemClickListener(catModel)
            }
        }


    }
}